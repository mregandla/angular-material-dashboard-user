/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardUser')

/**
 * @ngdoc controller
 * @name AmdGroupsCtrl
 * @description Manages list of groups
 * 
 */
.controller('AmdGroupsCtrl', function($scope, $usr, PaginatorParameter, $navigator, $translate) {

	var paginatorParameter = new PaginatorParameter();
	paginatorParameter.setOrder('id', 'a');
	var requests = null;
	var ctrl = {
			working: false,
			items: []
	};

	/**
	 * جستجوی درخواست‌ها
	 * 
	 * @param paginatorParameter
	 * @returns
	 */
	function find(query) {
		paginatorParameter.setQuery(query);
		reload();
	}

	/**
	 * لود کردن داده‌های صفحه بعد
	 * 
	 * @returns
	 */
	function nextPage() {
		if (ctrl.working) {
			return;
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			paginatorParameter.setPage(requests.next());
		}
		// start state (device list)
		ctrl.working = true;
		$usr.groups(paginatorParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
		})//
		.finally(function(){
			ctrl.working = false;
		});
	}


	/**
	 * درخواست مورد نظر را از سیستم حذف می‌کند.
	 * 
	 * @param request
	 * @returns
	 */
	function remove(pobject) {
		return confirm($translate.instant('Group will be removed. There is no undo.'))
		.then(function(){
			return pobject.delete()//
			.then(function(){
				var index = ctrl.items.indexOf(pobject);
				if (index > -1) {
					ctrl.items .splice(index, 1);
				}
			}, function(){
				alert($translate.instant('Failed to delete item.'));
			});
		});
		
	}

	/**
	 * تمام حالت‌های کنترل ررا بدوباره مقدار دهی می‌کند.
	 * 
	 * @returns
	 */
	function reload(){
		requests = null;
		ctrl.items = [];
		nextPage();
	}

	/**
	 * اضافه کردن گروه جدید
	 * 
	 * @returns
	 */
	function addGroup(){
		$navigator.openPage('/groups/new');
	}

	/*
	 * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری شده است.
	 */
	$scope.items = [];
	$scope.search = find;
	$scope.nextPage = nextPage;
	$scope.remove = remove;
	$scope.ctrl = ctrl;
	$scope.addGroup = addGroup;

	// Pagination toolbar
	$scope.paginatorParameter = paginatorParameter;
	$scope.reload = reload;
	$scope.sortKeys= [
		'id', 
		'name',
		'description'
		];
	$scope.sortKeysTitles= [
		'ID',
		'Name',
		'Description'
		];
	$scope.moreActions=[{
		title: 'New group',
		icon: 'group_add',
		action: addGroup
	}];

});
