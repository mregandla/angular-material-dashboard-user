/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardUser')

/**
 * @ngdoc controller
 * @name AmdUsersCtrl
 * @description Manages list of users
 */
.controller('AmdUsersCtrl', function($scope, $usr, $amdExport, PaginatorParameter, $navigator, $translate) {

	var paginatorParameter = new PaginatorParameter();
	paginatorParameter.setOrder('id', 'd');
	var requests = null;
	var ctrl = {
			state: 'relax',
			items: [],
			exporting: false
	};

	/**
	 * لود کردن داده‌های صفحه بعد
	 * 
	 * @returns
	 */
	function nextPage() {
		if (ctrl.status === 'working') {
			return;
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			paginatorParameter.setPage(requests.next());
		}
		// start state (device list)
		ctrl.status = 'working';
		$usr.users(paginatorParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
			ctrl.status = 'relax';
		}, function() {
			ctrl.status = 'fail';
		});
	}


	/**
	 * درخواست مورد نظر را از سیستم حذف می‌کند.
	 * 
	 * @param request
	 * @returns
	 */
	function remove(pobject) {
		if (!$scope.app.user.owner) {
			alert($translate.instant('You are not allowed to delete the activity.'));
			return;
		}
		confirm($translate.instant('User will be removed. There is no undo.'))
		.then(function() {
			pobject.delete()//
			.then(function(){
				var index = ctrl.items.indexOf(pobject);
				if (index > -1) {
					ctrl.items .splice(index, 1);
				}
			});
		});
	}

	/**
	 * تمام حالت‌های کنترل ررا بدوباره مقدار دهی می‌کند.
	 * 
	 * @returns
	 */
	function reload(){
		requests = null;
		ctrl.items = [];
		nextPage();
	}

	/**
	 * اضافه کردن گروه جدید
	 * 
	 * @returns
	 */
	function addUser(){
		$navigator.openPage('/users/new');
	}

	function exportList(){
		if (ctrl.exporting) {
			return;
		}
		ctrl.exporting = true;
		return $amdExport.list( $usr, $usr.users, paginatorParameter, 'csv', 'users')//
		.then(function(){
			ctrl.exporting = false;
		}, function(){
			ctrl.exporting = false;
			alert('Fail to export data.');
		});
		return 'ok';
	}

	/*
	 * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری
	 * شده است.
	 */
	$scope.items = [];
	$scope.nextPage = nextPage;
	$scope.remove = remove;
	$scope.ctrl = ctrl;
	$scope.addUser = addUser;

	// Pagination
	$scope.paginatorParameter = paginatorParameter;
	$scope.reload = reload;
	$scope.exportList = exportList;
	$scope.sortKeys= [
		'id', 
		'login',
		'first_name',
		'last_name',
		'last_login',
		'date_joined',
		];

	$scope.sortKeysTitles= [
		'ID',
		'Login',
		'First name',
		'Last name',
		'Last login',
		'Joined date',
		];
	$scope.moreActions=[
		{
			title: 'New user',
			icon: 'add',
			action: addUser
		},{
			title: 'Export',
			icon: 'save',
			action: exportList
		}
		];

});
