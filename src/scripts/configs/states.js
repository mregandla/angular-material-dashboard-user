/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardUser')
/*
 *  States of system
 */
.config(function($routeProvider) {
	$routeProvider //
	/**
	 * @ngdoc Routes
	 * @name /users
	 * @description List of users
	 * 
	 * Display and manages all system users.
	 */
	.when('/users', {
		controller : 'AmdUsersCtrl',
		templateUrl : 'views/amd-users.html',
		navigate : true,
		groups : [ 'user-management' ],
		name : 'Users',
		icon : 'person',
		/*
		 * @ngInject
		 */
		protect: function($rootScope){
			return !$rootScope.app.user.owner;
		},
		/*
		 * @ngInject
		 */
		integerate: function ($route, $actions){
			$actions.group('navigationPathMenu').clear();
			$actions.newAction({
				id: 'users',
				title: 'Users',
				type: 'link',
				priority : 10,
				visible : true,
				url: 'users',
				groups: ['navigationPathMenu'],
			});
		}
	})
	/**
	 * @ngdoc Routes
	 * @name /users/new
	 * @description Create a new user
	 * 
	 */
	.when('/users/new', {
		controller : 'AmdUserNewCtrl',
		templateUrl : 'views/amd-user-new.html',
		navigate : true,
		groups : [ 'user-management' ],
		name : 'New user',
		icon : 'person_add',
		/*
		 * @ngInject
		 */
		protect: function($rootScope){
			return !$rootScope.app.user.owner;
		},
		/*
		 * @ngInject
		 */
		integerate: function ($route, $actions){
			$actions.group('navigationPathMenu').clear();
			$actions.newAction({
				id: 'users',
				title: 'Users',
				type: 'link',
				priority : 10,
				visible : true,
				url: 'users',
				groups: ['navigationPathMenu'],
			});
			$actions.newAction({
				id: 'new-user',
				title: 'New user',
				type: 'link',
				priority : 10,
				visible : true,
				url: 'users/new',
				groups: ['navigationPathMenu'],
			});
		}
	})

	/**
	 * @ngdoc Routes
	 * @name /user/:id
	 * @description Details of a user
	 */
	.when('/user/:userId', {
		controller : 'AmdUserCtrl',
		templateUrl : 'views/amd-user.html',
		protect: true,
		/*
		 * @ngInject
		 */
		integerate: function ($route, $actions, $usr, $routeParams){
			$actions.group('navigationPathMenu').clear();
			$actions.newAction({
				id: 'users',
				title: 'Users',
				type: 'link',
				priority : 10,
				visible : true,
				url: 'users',
				groups: ['navigationPathMenu'],
			});
			$usr.user($routeParams.userId)//
			.then(function(user){
				var name = (user.first_name || user.last_name) ? user.first_name + ' ' + user.last_name : user.login; 
				$actions.newAction({
					id: 'user-' + user.id,
					title: name,
					type: 'link',
					priority : 10,
					visible : true,
					url: 'user/' + user.id,
					groups: ['navigationPathMenu'],
				});
			});
		}
	})


	/**
	 * @ngdoc Routes
	 * @name /groups
	 * @description List of grops
	 */
	.when('/groups', {
		templateUrl : 'views/amd-groups.html',
		controller : 'AmdGroupsCtrl',
		navigate : true,
		groups : [ 'user-management' ],
		name : 'Groups',
		icon : 'group',
		/*
		 * @ngInject
		 */
		protect: function($rootScope){
			return !$rootScope.app.user.owner;
		},
		/*
		 * @ngInject
		 */
		integerate: function ($route, $actions){
			$actions.group('navigationPathMenu').clear();
			$actions.newAction({
				id: 'groups',
				title: 'Groups',
				type: 'link',
				priority : 10,
				visible : true,
				url: 'groups',
				groups: ['navigationPathMenu'],
			});
		}
	})
	/**
	 * @ngdoc Routes
	 * @name /groups/new
	 * @description Create a new group
	 */
	.when('/groups/new', {
		templateUrl : 'views/amd-group-new.html',
		controller : 'AmdGroupNewCtrl',
		navigate : true,
		groups : [ 'user-management' ],
		name : 'New group',
		icon : 'group_add',
		/*
		 * @ngInject
		 */
		protect: function($rootScope){
			return !$rootScope.app.user.owner;
		},
		/*
		 * @ngInject
		 */
		integerate: function ($route, $actions){
			$actions.group('navigationPathMenu').clear();
			$actions.newAction({
				id: 'groups',
				title: 'Groups',
				type: 'link',
				priority : 10,
				visible : true,
				url: 'groups',
				groups: ['navigationPathMenu'],
			});
			$actions.newAction({
				id: 'new-group',
				title: 'New group',
				type: 'link',
				priority : 10,
				visible : true,
				url: 'groups/new',
				groups: ['navigationPathMenu'],
			});
		}
	})
	/**
	 * @ngdoc Routes
	 * @name /group/:groupId
	 * @description Group detail
	 */
	.when('/group/:groupId', {
		templateUrl : 'views/amd-group.html',
		controller : 'AmdGroupCtrl',
		protect: true,
		/*
		 * @ngInject
		 */
		integerate: function ($route, $actions, $usr, $routeParams){
			$actions.group('navigationPathMenu').clear();
			$actions.newAction({
				id: 'groups',
				title: 'Groups',
				type: 'link',
				priority : 10,
				visible : true,
				url: 'groups',
				groups: ['navigationPathMenu'],
			});
			$usr.group($routeParams.groupId)//
			.then(function(group){
				$actions.newAction({
					id: 'group-' + group.id,
					title: group.name,
					type: 'link',
					priority : 10,
					visible : true,
					url: 'group/' + group.id,
					groups: ['navigationPathMenu'],
				});
			});
		}
	})
	/**
	 * @ngdoc Routes
	 * @name /roles
	 * @description List of all users
	 */
	.when('/roles', {
		templateUrl : 'views/amd-roles.html',
		controller : 'AmdRolesCtrl',
		groups : [ 'user-management' ],
		navigate : true,
		name : 'Roles',
		icon : 'accessibility',
		/*
		 * @ngInject
		 */
		protect: function($rootScope){
			return !$rootScope.app.user.owner;
		},
		/*
		 * @ngInject
		 */
		integerate: function ($route, $actions){
			$actions.group('navigationPathMenu').clear();
			$actions.newAction({
				id: 'roles',
				title: 'Roles',
				type: 'link',
				priority : 10,
				visible : true,
				url: 'roles',
				groups: ['navigationPathMenu'],
			});
		}
	})
	/**
	 * @ngdoc Routes
	 * @name /role/:roleId
	 * @description Detalis of a role
	 */
	.when('/role/:roleId', {
		templateUrl : 'views/amd-role.html',
		controller : 'AmdRoleCtrl',
		protect: true,
		/*
		 * @ngInject
		 */
		integerate: function ($route, $actions, $usr, $routeParams){
			$actions.group('navigationPathMenu').clear();
			$actions.newAction({
				id: 'roles',
				title: 'Roles',
				type: 'link',
				priority : 10,
				visible : true,
				url: 'roles',
				groups: ['navigationPathMenu'],
			});
			$usr.role($routeParams.roleId)//
			.then(function(role){
				$actions.newAction({
					id: 'role-' + role.id,
					title: role.name,
					type: 'link',
					priority : 10,
					visible : true,
					url: 'role/' + role.id,
					groups: ['navigationPathMenu'],
				});
			});
		}
	});
});