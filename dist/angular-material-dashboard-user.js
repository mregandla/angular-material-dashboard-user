/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardUser', [ //
    'ngMaterialDashboard',//
]);

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardUser')
/*
 *  States of system
 */
.config(function($routeProvider) {
	$routeProvider //
	/**
	 * @ngdoc Routes
	 * @name /users
	 * @description List of users
	 * 
	 * Display and manages all system users.
	 */
	.when('/users', {
		controller : 'AmdUsersCtrl',
		templateUrl : 'views/amd-users.html',
		navigate : true,
		groups : [ 'user-management' ],
		name : 'Users',
		icon : 'person',
		/*
		 * @ngInject
		 */
		protect: function($rootScope){
			return !$rootScope.app.user.owner;
		},
		/*
		 * @ngInject
		 */
		integerate: function ($route, $actions){
			$actions.group('navigationPathMenu').clear();
			$actions.newAction({
				id: 'users',
				title: 'Users',
				type: 'link',
				priority : 10,
				visible : true,
				url: 'users',
				groups: ['navigationPathMenu'],
			});
		}
	})
	/**
	 * @ngdoc Routes
	 * @name /users/new
	 * @description Create a new user
	 * 
	 */
	.when('/users/new', {
		controller : 'AmdUserNewCtrl',
		templateUrl : 'views/amd-user-new.html',
		navigate : true,
		groups : [ 'user-management' ],
		name : 'New user',
		icon : 'person_add',
		/*
		 * @ngInject
		 */
		protect: function($rootScope){
			return !$rootScope.app.user.owner;
		},
		/*
		 * @ngInject
		 */
		integerate: function ($route, $actions){
			$actions.group('navigationPathMenu').clear();
			$actions.newAction({
				id: 'users',
				title: 'Users',
				type: 'link',
				priority : 10,
				visible : true,
				url: 'users',
				groups: ['navigationPathMenu'],
			});
			$actions.newAction({
				id: 'new-user',
				title: 'New user',
				type: 'link',
				priority : 10,
				visible : true,
				url: 'users/new',
				groups: ['navigationPathMenu'],
			});
		}
	})

	/**
	 * @ngdoc Routes
	 * @name /user/:id
	 * @description Details of a user
	 */
	.when('/user/:userId', {
		controller : 'AmdUserCtrl',
		templateUrl : 'views/amd-user.html',
		protect: true,
		/*
		 * @ngInject
		 */
		integerate: function ($route, $actions, $usr, $routeParams){
			$actions.group('navigationPathMenu').clear();
			$actions.newAction({
				id: 'users',
				title: 'Users',
				type: 'link',
				priority : 10,
				visible : true,
				url: 'users',
				groups: ['navigationPathMenu'],
			});
			$usr.user($routeParams.userId)//
			.then(function(user){
				var name = (user.first_name || user.last_name) ? user.first_name + ' ' + user.last_name : user.login; 
				$actions.newAction({
					id: 'user-' + user.id,
					title: name,
					type: 'link',
					priority : 10,
					visible : true,
					url: 'user/' + user.id,
					groups: ['navigationPathMenu'],
				});
			});
		}
	})


	/**
	 * @ngdoc Routes
	 * @name /groups
	 * @description List of grops
	 */
	.when('/groups', {
		templateUrl : 'views/amd-groups.html',
		controller : 'AmdGroupsCtrl',
		navigate : true,
		groups : [ 'user-management' ],
		name : 'Groups',
		icon : 'group',
		/*
		 * @ngInject
		 */
		protect: function($rootScope){
			return !$rootScope.app.user.owner;
		},
		/*
		 * @ngInject
		 */
		integerate: function ($route, $actions){
			$actions.group('navigationPathMenu').clear();
			$actions.newAction({
				id: 'groups',
				title: 'Groups',
				type: 'link',
				priority : 10,
				visible : true,
				url: 'groups',
				groups: ['navigationPathMenu'],
			});
		}
	})
	/**
	 * @ngdoc Routes
	 * @name /groups/new
	 * @description Create a new group
	 */
	.when('/groups/new', {
		templateUrl : 'views/amd-group-new.html',
		controller : 'AmdGroupNewCtrl',
		navigate : true,
		groups : [ 'user-management' ],
		name : 'New group',
		icon : 'group_add',
		/*
		 * @ngInject
		 */
		protect: function($rootScope){
			return !$rootScope.app.user.owner;
		},
		/*
		 * @ngInject
		 */
		integerate: function ($route, $actions){
			$actions.group('navigationPathMenu').clear();
			$actions.newAction({
				id: 'groups',
				title: 'Groups',
				type: 'link',
				priority : 10,
				visible : true,
				url: 'groups',
				groups: ['navigationPathMenu'],
			});
			$actions.newAction({
				id: 'new-group',
				title: 'New group',
				type: 'link',
				priority : 10,
				visible : true,
				url: 'groups/new',
				groups: ['navigationPathMenu'],
			});
		}
	})
	/**
	 * @ngdoc Routes
	 * @name /group/:groupId
	 * @description Group detail
	 */
	.when('/group/:groupId', {
		templateUrl : 'views/amd-group.html',
		controller : 'AmdGroupCtrl',
		protect: true,
		/*
		 * @ngInject
		 */
		integerate: function ($route, $actions, $usr, $routeParams){
			$actions.group('navigationPathMenu').clear();
			$actions.newAction({
				id: 'groups',
				title: 'Groups',
				type: 'link',
				priority : 10,
				visible : true,
				url: 'groups',
				groups: ['navigationPathMenu'],
			});
			$usr.group($routeParams.groupId)//
			.then(function(group){
				$actions.newAction({
					id: 'group-' + group.id,
					title: group.name,
					type: 'link',
					priority : 10,
					visible : true,
					url: 'group/' + group.id,
					groups: ['navigationPathMenu'],
				});
			});
		}
	})
	/**
	 * @ngdoc Routes
	 * @name /roles
	 * @description List of all users
	 */
	.when('/roles', {
		templateUrl : 'views/amd-roles.html',
		controller : 'AmdRolesCtrl',
		groups : [ 'user-management' ],
		navigate : true,
		name : 'Roles',
		icon : 'accessibility',
		/*
		 * @ngInject
		 */
		protect: function($rootScope){
			return !$rootScope.app.user.owner;
		},
		/*
		 * @ngInject
		 */
		integerate: function ($route, $actions){
			$actions.group('navigationPathMenu').clear();
			$actions.newAction({
				id: 'roles',
				title: 'Roles',
				type: 'link',
				priority : 10,
				visible : true,
				url: 'roles',
				groups: ['navigationPathMenu'],
			});
		}
	})
	/**
	 * @ngdoc Routes
	 * @name /role/:roleId
	 * @description Detalis of a role
	 */
	.when('/role/:roleId', {
		templateUrl : 'views/amd-role.html',
		controller : 'AmdRoleCtrl',
		protect: true,
		/*
		 * @ngInject
		 */
		integerate: function ($route, $actions, $usr, $routeParams){
			$actions.group('navigationPathMenu').clear();
			$actions.newAction({
				id: 'roles',
				title: 'Roles',
				type: 'link',
				priority : 10,
				visible : true,
				url: 'roles',
				groups: ['navigationPathMenu'],
			});
			$usr.role($routeParams.roleId)//
			.then(function(role){
				$actions.newAction({
					id: 'role-' + role.id,
					title: role.name,
					type: 'link',
					priority : 10,
					visible : true,
					url: 'role/' + role.id,
					groups: ['navigationPathMenu'],
				});
			});
		}
	});
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardUser')

/**
 * @ngdoc controller
 * @name AmdGroupNewCtrl
 * @description Creates new group
 */
.controller('AmdGroupNewCtrl', function($scope, $usr, $navigator) {
	var ctrl = {
			working: false
	};

	function cancel() {
		$navigator.openPage('/groups');
	}

	function addGroup(model) {
		ctrl.working = true;
		$usr.newGroup(model)//
		.then(function() {
			$navigator.openPage('/groups');
		}, function(error) {
			// Show error
		})//
		.finally(function(){
			ctrl.working = false;
		});
	}

	$scope.cancel = cancel;
	$scope.addGroup = addGroup;
	$scope.ctrl = ctrl;
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardUser')

/**
 * @ngdoc controller
 * @name AmdGroupCtrl
 * @property {boolean} groupLoading  Is true if controller is working on group.
 * @property {boolean} roleLoading   Is true if controller is working on roles.
 * @property {boolean} userLoading   Is true if controller is working on users.
 * @description Controller of a group
 * 
 * Manages a group view
 */
.controller('AmdGroupCtrl', function ($scope, $usr, $routeParams, $navigator, $resource, $translate, $q) {

	var ctrl = {
			roleLoading: true,
			groupLoading: true,
			userLoading: true
	}

	/**
	 * Remove the group
	 * 
	 * Remove current group from the backend.
	 * 
	 * @memberof AmdGroupCtrl
	 * @returns {promiss} to do 
	 */
	function remove() {
		return confirm($translate.instant('Group will be removed. There is no undo.'))
		.then(function(){
			return $scope.group.delete();//
		})//
		.then(function(){
			$navigator.openPage('/groups');
		}, function(error){
			alert($translate.instant('Failed to delete item.'));
		});
	}
	
	/**
	 * Save changes of the current group
	 * 
	 * Save the current group to the backend.
	 * 
	 * @memberof AmdGroupCtrl
	 * @return {promiss} to do 
	 */
	function save(){
		if(ctrl.groupLoading){
			return;
		}
		ctrl.groupLoading = true;
		return $scope.group.update()//
		.then(function(){
			toast($translate.instant('Save is successfull.'));
		})//
		.finally(function(){
			ctrl.groupLoading = false;
		});
	}

	function loadRoles(){
		ctrl.roleLoading = true;
		return $scope.group.roles()//
		.then(function(roles){
			$scope.roles = roles;
		})//
		.finally(function(){
			ctrl.roleLoading = false;
		});
	}

	function loadUsers(){
		ctrl.userLoading = true;
		return $scope.group.users()//
		.then(function(users){
			$scope.users = users;
		})//
		.finally(function(){
			ctrl.userLoading = false;
		});
	}

	function load() {
		ctrl.groupLoading = true;
		return $usr.group($routeParams.groupId)//
		.then(function(group){
			$scope.group = group;
			loadRoles();
			loadUsers();
		})//
		.finally(function(){
			ctrl.groupLoading = false;
		});
	}

	function changeRoles(){
		var myData = $scope.roles ? $scope.roles.items : [];
		return $resource.get('role-list', {
			data: myData
		})//
		.then(function(list){
			// change roles and reload roles
			var jobs = [];
			list.forEach(function(item){
				if(_findIndex(myData, item) < 0){
					var promise = $scope.group.newRole({
						'id': item.id,
						'role': item.id,
						'role_id': item.id
					});
					jobs.push(promise);
				}
			});
			myData.forEach(function(item){
				if(_findIndex(list, item) < 0){
					var promise = $scope.group.removeRole(item);
					jobs.push(promise);
				}
			});
			$q.all(jobs)//
			.then(function(){
				loadRoles();
			}, function(){
				$scope.roles = myData;
				alert($translate.instant('An error occured while set roles.'));
			});
		});
	}
	
	function changeUsers(){
		var myData = $scope.users ? $scope.users.items : [];
		return $resource.get('user-list', {
			data: myData
		})//
		.then(function(list){
			// change users and reload users
			var jobs = [];
			list.forEach(function(item){
				if(_findIndex(myData, item) < 0){
					var promise = $scope.group.newUser({
						'id': item.id,
						'user': item.id,
						'user_id': item.id,
					});
					jobs.push(promise);
				}
			});
			myData.forEach(function(item){
				if(_findIndex(list, item) < 0){
					var promise = $scope.group.removeUser(item);
					jobs.push(promise);
				}
			});
			$q.all(jobs)//
			.then(function(){
				loadUsers();
			}, function(){
				$scope.users = myData;
				alert($translate.instant('An error occured while set users.'));
			});
		});
	}
	
	function _findIndex(array, item){
		for(var i=0; i <array.length; i++){
			if(array[i].id === item.id){
				return i;
			}
		}
		return -1;
	}
	
	function removeRole(role) {
		if(ctrl.roleLoading){
			return;
		}
		ctrl.roleLoading = true;
		confirm($translate.instant('Item will be deleted.'))//
		.then(function(){
		    return $scope.group.removeRole(role);
		})//
		.then(function(){
			var index = $scope.roles.items.indexOf(role);
			if (index > -1) {
				$scope.roles.items.splice(index, 1);
			}
		})//
		.finally(function(){
			ctrl.roleLoading = false;
		});
	}
	
	function removeUser(user) {
		if(ctrl.userLoading){
			return;
		}
		ctrl.userLoading = true;
		confirm($translate.instant('Item will be deleted.'))//
		.then(function(){
		    return $scope.group.removeUser(user);
		})//
		.then(function(){
			var index = $scope.users.items.indexOf(user);
			if (index > -1) {
				$scope.users.items.splice(index, 1);
			}
		})//
		.finally(function(){
			ctrl.userLoading = false;
		});
	}
	
	$scope.save = save;
	$scope.remove = remove;
	
	$scope.changeRoles = changeRoles;
	$scope.changeUsers = changeUsers;
	
	$scope.removeRole = removeRole;
	$scope.removeUser = removeUser;
	
	$scope.ctrl = ctrl;
	load();
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardUser')

/**
 * @ngdoc controller
 * @name AmdGroupsOfCtrl
 * @description Creates new user
 */
.controller('AmdGroupsOfCtrl', function($scope, $usr, $routeParams, $navigator, $resource, PaginatorParameter) {

	var paginatorParameter = new PaginatorParameter();
	var requests = null;
	var ctrl = {
			state: 'relax',
			items: [],
			working: false
	};

	function handleError(error){
		var message ='';
		if(error.data){
			message = error.data.message;
		}
		alert('Fail to change the group:'+message);
		ctrl.working = false;
	}

	/**
	 * جستجوی درخواست‌ها
	 * 
	 * @param paginatorParameter
	 * @returns
	 */
	function find(query) {
		paginatorParameter.setQuery(query);
		reload();
	}

	/**
	 * لود کردن داده‌های صفحه بعد
	 * 
	 * @returns
	 */
	function nextPage() {
		if (ctrl.working || ! $scope.object) {
			return;
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			paginatorParameter.setPage(requests.next());
		}
		// start state (device list)
		ctrl.working = true;
		$scope.object.groups(paginatorParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
		},handleError)//
		.finally(function(){
			ctrl.working = false;
		});
	}

	/**
	 * درخواست مورد نظر را از سیستم حذف می‌کند.
	 * 
	 * @param request
	 * @returns
	 */
	function remove(group) {
		if (ctrl.status === 'working') {
			return;
		}
		ctrl.status = 'working';
		$scope.object//
		.removeGroup(group)//
		.then(function(){
			var index = ctrl.items.indexOf(group);
			if (index > -1) {
				ctrl.items .splice(index, 1);
			}
			ctrl.status = 'relax';
		}, handleError);
	}

	function _internal_reload(){
		requests = null;
		ctrl.items = [];
		nextPage();
	}

	/**
	 * تمام حالت‌های کنترل ررا بدوباره مقدار دهی می‌کند.
	 * 
	 * @returns
	 */
	function reload(){
		if($routeParams.objectType === 'user'){
			return $usr.user($routeParams.objectId)//
			.then(function(user){
				$scope.object = user;
				_internal_reload();
			});
		} else if($routeParams.objectType === 'role') {
			return $usr.role($routeParams.objectId)//
			.then(function(role){
				$scope.object = role;
				_internal_reload();
			});
		}
	}

	/**
	 * Adding new group into the current object
	 */
	function addGroup(){
		ctrl.working = true;
		$resource.get('groupId')//
		.then(function(groupId){
			return $scope.object.newGroup({
				'id': groupId,
				'group': groupId,
				'group_id': groupId,
			});
		})//
		.then(function(){
			_internal_reload();
		}, handleError);
	}

	/*
	 * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری شده است.
	 */
	$scope.reload = reload;
	$scope.search = find;
	$scope.nextPage = nextPage;

	$scope.remove = remove;
	$scope.addGroup = addGroup;

	$scope.ctrl = ctrl;
	$scope.paginatorParameter = paginatorParameter;
	$scope.sortKeys = [
		'id', 
		'name'
		];
	$scope.sortKeysTitles = [
		'ID',
		'Name'
	];
	$scope.moreActions = [{
		title: 'New group',
		icon: 'add',
		action: addGroup 
	}];
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardUser')

/**
 * @ngdoc controller
 * @name AmdGroupsCtrl
 * @description Manages list of groups
 * 
 */
.controller('AmdGroupsCtrl', function($scope, $usr, PaginatorParameter, $navigator, $translate) {

	var paginatorParameter = new PaginatorParameter();
	paginatorParameter.setOrder('id', 'a');
	var requests = null;
	var ctrl = {
			working: false,
			items: []
	};

	/**
	 * جستجوی درخواست‌ها
	 * 
	 * @param paginatorParameter
	 * @returns
	 */
	function find(query) {
		paginatorParameter.setQuery(query);
		reload();
	}

	/**
	 * لود کردن داده‌های صفحه بعد
	 * 
	 * @returns
	 */
	function nextPage() {
		if (ctrl.working) {
			return;
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			paginatorParameter.setPage(requests.next());
		}
		// start state (device list)
		ctrl.working = true;
		$usr.groups(paginatorParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
		})//
		.finally(function(){
			ctrl.working = false;
		});
	}


	/**
	 * درخواست مورد نظر را از سیستم حذف می‌کند.
	 * 
	 * @param request
	 * @returns
	 */
	function remove(pobject) {
		return confirm($translate.instant('Group will be removed. There is no undo.'))
		.then(function(){
			return pobject.delete()//
			.then(function(){
				var index = ctrl.items.indexOf(pobject);
				if (index > -1) {
					ctrl.items .splice(index, 1);
				}
			}, function(){
				alert($translate.instant('Failed to delete item.'));
			});
		});
		
	}

	/**
	 * تمام حالت‌های کنترل ررا بدوباره مقدار دهی می‌کند.
	 * 
	 * @returns
	 */
	function reload(){
		requests = null;
		ctrl.items = [];
		nextPage();
	}

	/**
	 * اضافه کردن گروه جدید
	 * 
	 * @returns
	 */
	function addGroup(){
		$navigator.openPage('/groups/new');
	}

	/*
	 * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری شده است.
	 */
	$scope.items = [];
	$scope.search = find;
	$scope.nextPage = nextPage;
	$scope.remove = remove;
	$scope.ctrl = ctrl;
	$scope.addGroup = addGroup;

	// Pagination toolbar
	$scope.paginatorParameter = paginatorParameter;
	$scope.reload = reload;
	$scope.sortKeys= [
		'id', 
		'name',
		'description'
		];
	$scope.sortKeysTitles= [
		'ID',
		'Name',
		'Description'
		];
	$scope.moreActions=[{
		title: 'New group',
		icon: 'group_add',
		action: addGroup
	}];

});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardUser')


/**
 * @ngdoc controller
 * @name AmdRoleCtrl
 * @property {boolean} groupLoading  Is true if controller is working on groups.
 * @property {boolean} roleLoading   Is true if controller is working on role.
 * @property {boolean} userLoading   Is true if controller is working on users.
 * @description Manages a role view
 * 
 */
.controller('AmdRoleCtrl', function ($scope, $usr, $routeParams, $navigator, $resource, $translate, $q) {

	var ctrl = {
			roleLoading: true,
			groupLoading: true,
			userLoading: true
	}

	/**
	 * Remove current role from server
	 *  
	 * @memberof AmdRoleCtrl
	 * @return {promiss} to do the remove process
	 */
	function remove() {
		return confirm($translate.instant('Role will be removed. There is no undo.'))
		.then(function(){
			return $scope.role.delete();//
		})//
		.then(function(){
			$navigator.openPage('/roles');
		}, function(error){
			alert($translate.instant('Failed to delete item.'));
		});
	}
	
	/**
	 * Save current role
	 * 
	 * Save all changes to the current role.
	 * 
	 * @memberof AmdRoleCtrl
	 * @return {promiss} to save changes
	 */
	function save(){
		if(ctrl.roleLoading){
			return;
		}
		ctrl.roleLoading = true;
		return $scope.role.update()//
		.then(function(){
			toast($translate.instant('Save is successfull.'));
		})//
		.finally(function(){
			ctrl.roleLoading = false;
		});
	}
	
	function loadGroups(){
		ctrl.groupLoading = true;
		return $scope.role.groups()//
		.then(function(groups){
			$scope.groups = groups;
		})//
		.finally(function(){
			ctrl.groupLoading = false;
		});
	}

	function loadUsers(){
		ctrl.userLoading = true;
		return $scope.role.users()//
		.then(function(users){
			$scope.users = users;
		})//
		.finally(function(){
			ctrl.userLoading = false;
		});
	}

	function load() {
		ctrl.roleLoading = true;
		return $usr.role($routeParams.roleId)//
		.then(function(role){
			$scope.role = role;
			loadGroups();
			loadUsers();
		})//
		.finally(function(){
			ctrl.roleLoading = true;
		});
	}

	function changeGroups(){
		var myData = $scope.groups ? $scope.groups.items : [];
		return $resource.get('group-list', {
			data: myData
		})//
		.then(function(list){
			// change groups and reload groups
			var jobs = [];
			list.forEach(function(item){
				if(_findIndex(myData, item) < 0){
					var promise = $scope.role.newGroup({
						'id': item.id,
						'group': item.id,
						'group_id': item.id,
					});
					jobs.push(promise);
				}
			});
			myData.forEach(function(item){
				if(_findIndex(list, item) < 0){
					var promise = $scope.role.removeGroup(item);
					jobs.push(promise);
				}
			});
			$q.all(jobs)//
			.then(function(){
				loadGroups();
			}, function(){
				$scope.groups = myData;
				alert($translate.instant('An error occured while set groups.'));
			});
		});
	}
	
	function changeUsers(){
		var myData = $scope.users ? $scope.users.items : [];
		return $resource.get('user-list', {
			data: myData
		})//
		.then(function(list){
			// change users and reload users
			var jobs = [];
			list.forEach(function(item){
				if(_findIndex(myData, item) < 0){
					var promise = $scope.role.newUser({
						'id': item.id,
						'user': item.id,
						'user_id': item.id,
					});
					jobs.push(promise);
				}
			});
			myData.forEach(function(item){
				if(_findIndex(list, item) < 0){
					var promise = $scope.role.removeUser(item);
					jobs.push(promise);
				}
			});
			$q.all(jobs)//
			.then(function(){
				loadUsers();
			}, function(){
				$scope.users = myData;
				alert($translate.instant('An error occured while set users.'));
			});
		});
	}
	
	function _findIndex(array, item){
		for(var i=0; i <array.length; i++){
			if(array[i].id === item.id){
				return i;
			}
		}
		return -1;
	}
	
	function removeGroup(group) {
		if(ctrl.groupLoading){
			return;
		}
		ctrl.groupLoading = true;
		confirm($translate.instant('Item will be deleted.'))//
		.then(function(){
		    return $scope.role.removeGroup(group);
		})//
		.then(function(){
			var index = $scope.groups.items.indexOf(group);
			if (index > -1) {
				$scope.groups.items.splice(index, 1);
			}
		})//
		.finally(function(){
			ctrl.groupLoading = false;
		});
	}
	
	function removeUser(user) {
		if(ctrl.userLoading){
			return;
		}
		ctrl.userLoading = true;
		confirm($translate.instant('Item will be deleted.'))//
		.then(function(){
		    return $scope.role.removeUser(user);
		})//
		.then(function(){
			var index = $scope.users.items.indexOf(user);
			if (index > -1) {
				$scope.users.items.splice(index, 1);
			}
		})//
		.finally(function(){
			ctrl.userLoading = false;
		});
	}
	
	$scope.save = save;
	$scope.remove = remove;
	
	$scope.changeGroups = changeGroups;
	$scope.changeUsers = changeUsers;
	
	$scope.removeGroup = removeGroup;
	$scope.removeUser = removeUser;
	
	$scope.ctrl = ctrl;
	load();
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardUser')


/**
 * @ngdoc controller
 * @name AmdRolesOfCtrl
 * @description Creates new user
 */
.controller('AmdRolesOfCtrl', function($scope, $usr, $routeParams, $navigator, $resource, PaginatorParameter) {

	var paginatorParameter = new PaginatorParameter();
	var requests = null;
	var ctrl = {
			items: [],
			working: false
	};

	function handleError(error){
		var message ='';
		if(error.data){
			message = error.data.message;
		}
		alert('Fail to change the role:'+message);
		ctrl.working = false;;
	}

	/**
	 * جستجوی درخواست‌ها
	 * 
	 * @param paginatorParameter
	 * @returns
	 */
	function find(query) {
		paginatorParameter.setQuery(query);
		reload();
	}

	/**
	 * لود کردن داده‌های صفحه بعد
	 * 
	 * @returns
	 */
	function nextPage() {
		if (ctrl.working) {
			return;
		}
		if(!$scope.object){
			return reload();
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			paginatorParameter.setPage(requests.next());
		}
		// start state (device list)
		ctrl.working = true;
		$scope.object.roles(paginatorParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
		},handleError)//
		.finally(function(){
			ctrl.working = false;
		});
	}

	/**
	 * درخواست مورد نظر را از سیستم حذف می‌کند.
	 * 
	 * @param request
	 * @returns
	 */
	function remove(role) {
		if(ctrl.working){
			return;
		}
		confirm('delete role from this list?')//
		.then(function(){
			ctrl.working = true;
		    return $scope.object.removeRole(role);
		})//
		.then(function(){
			var index = ctrl.items.indexOf(role);
			if (index > -1) {
				ctrl.items .splice(index, 1);
			}
		}, handleError)//
		.finally(function(){
			ctrl.working = false;
		});
	}

	function _internal_reload(){
		requests = null;
		ctrl.items = [];
		nextPage();
	}

	/**
	 * تمام حالت‌های کنترل ررا بدوباره مقدار دهی می‌کند.
	 * 
	 * @returns
	 */
	function reload(){
		if($routeParams.objectType === 'user'){
			return $usr.user($routeParams.objectId)//
			.then(function(user){
				$scope.object = user;
				_internal_reload();
			});
		} else if($routeParams.objectType === 'group'){
			return $usr.group($routeParams.objectId)//
			.then(function(group){
				$scope.object = group;
				_internal_reload();
			});
		}
	}

	/**
	 * Adding role into the current object
	 */
	function addRole(){
		if(ctrl.working){
			return;
		}
		ctrl.working = true;
		$resource.get('roleId')//
		.then(function(roleId){
			return $scope.object.newRole({
				'id': roleId,
				'role': roleId,
				'role_id': roleId
			});
		})//
		.then(function(){
			ctrl.working = false;
			_internal_reload();
		}, handleError);
	}

	/*
	 * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری شده است.
	 */
	$scope.reload = reload;
	$scope.search = find;
	$scope.nextPage = nextPage;

	$scope.remove = remove;
	$scope.add = addRole;

	$scope.ctrl = ctrl;
	$scope.paginatorParameter = paginatorParameter;
	$scope.sortKeys = [
		'id', 
		'name'
		];
	$scope.sortKeysTitles = [
		'ID',
		'Name'
		];
	$scope.moreActions = [{
		title: 'New role',
		icon: 'add',
		action: addRole 
	}];
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardUser')

/**
 * @ngdoc controller
 * @name AmdRolesCtrl
 * @description Creates new user
 */
.controller('AmdRolesCtrl', function($scope, $usr, PaginatorParameter ) {

	var paginatorParameter = new PaginatorParameter();
	paginatorParameter.setOrder('id', 'a');
	var requests = null;
	var ctrl = {
			state: 'relax',
			items: []
	};

//	/**
//	* جستجوی درخواست‌ها
//	* 
//	* @param paginatorParameter
//	* @returns
//	*/
//	function find(query) {
//	paginatorParameter.setQuery(query);
//	reload();
//	}

	/**
	 * لود کردن داده‌های صفحه بعد
	 * 
	 * @returns
	 */
	function nextPage() {
		if (ctrl.status === 'working') {
			return;
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			paginatorParameter.setPage(requests.next());
		}
		// start state (device list)
		ctrl.status = 'working';
		$usr.roles(paginatorParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
			ctrl.status = 'relax';
		}, function() {
			ctrl.status = 'fail';
		});
	}


	/**
	 * درخواست مورد نظر را از سیستم حذف می‌کند.
	 * 
	 * @param request
	 * @returns
	 */
	function remove(pobject) {
		return confirm($translate.instant('Role will be removed. There is no undo.'))
		.then(function(){
			return pobject.delete()//
			.then(function(){
				var index = ctrl.items.indexOf(pobject);
				if (index > -1) {
					ctrl.items .splice(index, 1);
				}
			}, function(){
				alert($translate.instant('Failed to delete item.'));
			});
		});

	}

	/**
	 * تمام حالت‌های کنترل ررا بدوباره مقدار دهی می‌کند.
	 * 
	 * @returns
	 */
	function reload(){
		requests = null;
		ctrl.items = [];
		nextPage();
	}

	/*
	 * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری
	 * شده است.
	 */
	$scope.items = [];
	$scope.reload = reload;
	$scope.search = find;
	$scope.nextPage = nextPage;
	$scope.remove = remove;
	$scope.ctrl = ctrl;
	$scope.paginatorParameter = paginatorParameter;
	$scope.sortKeys = [
		'id', 
		'name'
		];
	$scope.sortKeysTitles = [
		'ID',
		'Name'
		];
	$scope.moreActions = [{
		title: 'New role',
		icon: 'add',
		action: function(){
			alert('well done');
		}
	}];

	/*
	 * مقداردهی اولیه
	 */
//	reload();
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardUser')

/**
 * @ngdoc controller
 * @name AmdUserNewCtrl
 * @description Creates new user
 */
.controller('AmdUserNewCtrl', function($scope, $usr, $navigator, $errorHandler) {

	/*
	 * View controller options
	 */
	var ctrl = {
			working: false
	}

	function cancel() {
		$navigator.openPage('/users');
	}

	function addUser(model) {
		ctrl.working = true;
		$usr.newUser(model)//
		.then(function(user) {
			$navigator.openPage('/users');
			$scope.errorMessage = null;
		}, function(error) {
			$scope.errorMessage = $errorHandler.handleError(error, ctrl.myForm);
		})//
		.finally(function(){
			ctrl.working = false;
		});
	}
	
	$scope.cancel = cancel;
	$scope.addUser = addUser;
	$scope.ctrl = ctrl;
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardUser')


/**
 * @ngdoc controller
 * @name AmdUserCtrl
 * @description Creates new user
 */
.controller('AmdUserCtrl', function ($scope, $usr, $routeParams, $navigator, $resource, $translate, $q) {

	var ctrl = {
			roleLoading: true,
			groupLoading: true,
			userLoading: true
	}
	/**
	 * درخواست مورد نظر را از سیستم حذف می‌کند.
	 * 
	 * @param request
	 * @returns
	 */
	function remove() {
		confirm($translate.instant('User will be deleted. There is no undo.'))//
		.then(function(){
			return $scope.user.delete();//
		})//
		.then(function(){
			$navigator.openPage('/users');
		}, function(){
			alert($translate.instant('Failed to delete item.'));
		});
	}
	
	function update(){
		if(ctrl.userLoading){
			return;
		}
		ctrl.userLoading = true;
		return $scope.user.update()//
		.then(function(){
			toast($translate.instant('Save is successfull.'));
		})//
		.finally(function(){
			ctrl.userLoading = false;
		});
	}

	/**
	 * Removed all roles of user
	 */
	function removeRoles() {
		confirm($translate.instant('All roles of user will be removed.'))//
		.then(function(){
			var jobs = [];
			$scope.roles.items.forEach(function(item){
				var promise = $scope.user.removeRole(item);
				jobs.push(promise);
			});
			return $q.all(jobs);
		})//
		.then(function(){
			$navigator.openPage('/users');
		});
	}

	function loadRoles(){
		ctrl.roleLoading = true;
		return $scope.user.roles()//
		.then(function(roles){
			$scope.roles = roles;
		})//
		.finally(function(){
			ctrl.roleLoading = false;
		});
	}
	
	function loadGroups(){
		ctrl.groupLoading = true;
		return $scope.user.groups()//
		.then(function(groups){
			$scope.groups = groups;
		})//
		.finally(function(){
			ctrl.groupLoading = false;
		});
	}

	function load() {
		ctrl.userLoading = true;
		return $usr.user($routeParams.userId)//
		.then(function(user){
			$scope.user = user;
			loadRoles();
			loadGroups();
		})//
		.finally(function(){
			ctrl.userLoading = false;
		});
	}

	function changeRoles(){
		var myData = $scope.roles ? $scope.roles.items : [];
		return $resource.get('role-list', {
			data: myData
		})//
		.then(function(list){
			// change roles and reload roles
			var jobs = [];
			list.forEach(function(item){
				if(_findIndex(myData, item) < 0){
					var promise = $scope.user.newRole({
						'id': item.id,
						'role': item.id,
						'role_id': item.id
					});
					jobs.push(promise);
				}
			});
			myData.forEach(function(item){
				if(_findIndex(list, item) < 0){
					var promise = $scope.user.removeRole(item);
					jobs.push(promise);
				}
			});
			$q.all(jobs)//
			.then(function(){
				loadRoles();
			}, function(){
				$scope.roles = myData;
				alert($translate.instant('An error occured while set roles.'));
			});
		});
	}
	
	function changeGroups(){
		var myData = $scope.groups ? $scope.groups.items : [];
		return $resource.get('group-list', {
			data: myData
		})//
		.then(function(list){
			// change groups and reload groups
			var jobs = [];
			list.forEach(function(item){
				if(_findIndex(myData, item) < 0){
					var promise = $scope.user.newGroup({
						'id': item.id,
						'group': item.id,
						'group_id': item.id,
					});
					jobs.push(promise);
				}
			});
			myData.forEach(function(item){
				if(_findIndex(list, item) < 0){
					var promise = $scope.user.removeGroup(item);
					jobs.push(promise);
				}
			});
			$q.all(jobs)//
			.then(function(){
				loadGroups();
			}, function(){
				$scope.groups = myData;
				alert($translate.instant('An error occured while set groups.'));
			});
		});
	}
	
	function _findIndex(array, item){
		for(var i=0; i <array.length; i++){
			if(array[i].id === item.id){
				return i;
			}
		}
		return -1;
	}
	
	function removeRole(role) {
		if(ctrl.roleLoading){
			return;
		}
		ctrl.roleLoading = true;
		confirm($translate.instant('Item will be deleted.'))//
		.then(function(){
		    return $scope.user.removeRole(role);
		})//
		.then(function(){
			var index = $scope.roles.items.indexOf(role);
			if (index > -1) {
				$scope.roles.items.splice(index, 1);
			}
		})//
		.finally(function(){
			ctrl.roleLoading = false;
		});
	}
	
	function removeGroup(group) {
		if(ctrl.groupLoading){
			return;
		}
		ctrl.groupLoading = true;
		confirm($translate.instant('Item will be deleted.'))//
		.then(function(){
		    return $scope.user.removeGroup(group);
		})//
		.then(function(){
			var index = $scope.groups.items.indexOf(group);
			if (index > -1) {
				$scope.groups.items.splice(index, 1);
			}
		})//
		.finally(function(){
			ctrl.groupLoading = false;
		});
	}
	
	$scope.remove = remove;
	$scope.update = update;
	$scope.removeRoles = removeRoles;
	
	$scope.changeRoles = changeRoles;
	$scope.changeGroups = changeGroups;
	
	$scope.removeRole = removeRole;
	$scope.removeGroup = removeGroup;
	
	$scope.ctrl = ctrl;
	load();
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardUser')
/**
 * @ngdoc controller
 * @name AmdUsersOfCtrl
 * @description Manages list of user from an object
 */
.controller('AmdUsersOfCtrl', function($scope, $usr, $routeParams, $navigator, $resource, PaginatorParameter) {

	var paginatorParameter = new PaginatorParameter();
	var requests = null;
	var ctrl = {
			items: [],
			working: false
	};

	function handleError(error){
		var message ='';
		if(error.data){
			message = error.data.message;
		}
		alert('Fail to change:'+message);
		ctrl.working = false;
	}

	/**
	 * جستجوی درخواست‌ها
	 * 
	 * @param paginatorParameter
	 * @returns
	 */
	function find(query) {
		paginatorParameter.setQuery(query);
		reload();
	}

	/**
	 * لود کردن داده‌های صفحه بعد
	 * 
	 * @returns
	 */
	function nextPage() {
		if (ctrl.working || ! $scope.object) {
			return;
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			paginatorParameter.setPage(requests.next());
		}
		// start state (device list)
		ctrl.working = true;
		$scope.object.users(paginatorParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
		},handleError)//
		.finally(function(){
			ctrl.working = false;
		});
	}

	/**
	 * درخواست مورد نظر را از سیستم حذف می‌کند.
	 * 
	 * @param request
	 * @returns
	 */
	function remove(user) {
		if (ctrl.working) {
			return;
		}
		confirm('delete user from this list?')//
		.then(function(){
			ctrl.working = true;
			return $scope.object.removeUser(user);
		})//
		.then(function(){
			var index = ctrl.items.indexOf(user);
			if (index > -1) {
				ctrl.items .splice(index, 1);
			}
		}, handleError)//
		.finally(function(){
			ctrl.working = false;
		});
	}

	function _internal_reload(){
		requests = null;
		ctrl.items = [];
		nextPage();
	}

	/**
	 * تمام حالت‌های کنترل ررا بدوباره مقدار دهی می‌کند.
	 * 
	 * @returns
	 */
	function reload(){
		if($routeParams.objectType === 'group'){
			return $usr.group($routeParams.objectId)//
			.then(function(group){
				$scope.object = group;
				_internal_reload();
			});
		} else if($routeParams.objectType === 'role') {
			return $usr.role($routeParams.objectId)//
			.then(function(role){
				$scope.object = role;
				_internal_reload();
			});
		}
	}

	/**
	 * Add new user to the object
	 */
	function addUser(){
		ctrl.working = true;
		$resource.get('userId')//
		.then(function(userId){
			return $scope.object.newUser({
				'id': userId,
				'user': userId,
				'user_id': userId,
			});
		})//
		.then(function(){
			ctrl.working = false;
			_internal_reload();
		}, handleError);
	}

	/*
	 * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری شده است.
	 */
	$scope.reload = reload;
	$scope.search = find;
	$scope.nextPage = nextPage;

	$scope.remove = remove;
	$scope.addUser = addUser;

	$scope.ctrl = ctrl;
	$scope.paginatorParameter = paginatorParameter;
	$scope.sortKeys = [
		'id', 
		'name'
		];
	$scope.sortKeysTitles = [
		'ID',
		'Name'
	];
	$scope.moreActions = [{
		title: 'New user',
		icon: 'add',
		action: addUser 
	}];
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardUser')

/**
 * @ngdoc controller
 * @name AmdUsersCtrl
 * @description Manages list of users
 */
.controller('AmdUsersCtrl', function($scope, $usr, $amdExport, PaginatorParameter, $navigator, $translate) {

	var paginatorParameter = new PaginatorParameter();
	paginatorParameter.setOrder('id', 'd');
	var requests = null;
	var ctrl = {
			state: 'relax',
			items: [],
			exporting: false
	};

	/**
	 * لود کردن داده‌های صفحه بعد
	 * 
	 * @returns
	 */
	function nextPage() {
		if (ctrl.status === 'working') {
			return;
		}
		if (requests && !requests.hasMore()) {
			return;
		}
		if (requests) {
			paginatorParameter.setPage(requests.next());
		}
		// start state (device list)
		ctrl.status = 'working';
		$usr.users(paginatorParameter)//
		.then(function(items) {
			requests = items;
			ctrl.items = ctrl.items.concat(requests.items);
			ctrl.status = 'relax';
		}, function() {
			ctrl.status = 'fail';
		});
	}


	/**
	 * درخواست مورد نظر را از سیستم حذف می‌کند.
	 * 
	 * @param request
	 * @returns
	 */
	function remove(pobject) {
		if (!$scope.app.user.owner) {
			alert($translate.instant('You are not allowed to delete the activity.'));
			return;
		}
		confirm($translate.instant('User will be removed. There is no undo.'))
		.then(function() {
			pobject.delete()//
			.then(function(){
				var index = ctrl.items.indexOf(pobject);
				if (index > -1) {
					ctrl.items .splice(index, 1);
				}
			});
		});
	}

	/**
	 * تمام حالت‌های کنترل ررا بدوباره مقدار دهی می‌کند.
	 * 
	 * @returns
	 */
	function reload(){
		requests = null;
		ctrl.items = [];
		nextPage();
	}

	/**
	 * اضافه کردن گروه جدید
	 * 
	 * @returns
	 */
	function addUser(){
		$navigator.openPage('/users/new');
	}

	function exportList(){
		if (ctrl.exporting) {
			return;
		}
		ctrl.exporting = true;
		return $amdExport.list( $usr, $usr.users, paginatorParameter, 'csv', 'users')//
		.then(function(){
			ctrl.exporting = false;
		}, function(){
			ctrl.exporting = false;
			alert('Fail to export data.');
		});
		return 'ok';
	}

	/*
	 * تمام امکاناتی که در لایه نمایش ارائه می‌شود در اینجا نام گذاری
	 * شده است.
	 */
	$scope.items = [];
	$scope.nextPage = nextPage;
	$scope.remove = remove;
	$scope.ctrl = ctrl;
	$scope.addUser = addUser;

	// Pagination
	$scope.paginatorParameter = paginatorParameter;
	$scope.reload = reload;
	$scope.exportList = exportList;
	$scope.sortKeys= [
		'id', 
		'login',
		'first_name',
		'last_name',
		'last_login',
		'date_joined',
		];

	$scope.sortKeysTitles= [
		'ID',
		'Login',
		'First name',
		'Last name',
		'Last login',
		'Joined date',
		];
	$scope.moreActions=[
		{
			title: 'New user',
			icon: 'add',
			action: addUser
		},{
			title: 'Export',
			icon: 'save',
			action: exportList
		}
		];

});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardUser')
.run(function($navigator) {
	$navigator
	/**
	 * @ngdoc setting-group
	 * @name user-management
	 * @description List of user management tools
	 * 
	 * Group all user management tools under a group.
	 */
	.newGroup({
		id: 'user-management',
		title: 'User management',
		description: 'A module of dashboard to manage users.',
		icon: 'supervisor_account',
		priority: 5
	});
});
angular.module('ngMaterialDashboardUser').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/amd-group-new.html',
    " <form flex layout=column ng-submit=addGroup(model) mb-preloading=ctrl.working> <input hide type=\"submit\"> <div layout=column layout-padding> <md-input-container> <label translate>Name</label> <input ng-model=model.name> </md-input-container> <md-input-container> <label translate>Description</label> <input ng-model=model.description> </md-input-container> </div> <div layout=row> <span flex></span> <md-button class=md-raised ng-click=cancel() aria-label=close> <wb-icon>close</wb-icon> <span translate>Cancle</span> </md-button> <md-button class=md-raised ng-click=addGroup(model)> <wb-icon>done</wb-icon> <span translate>Add</span> </md-button> </div> </form>"
  );


  $templateCache.put('views/amd-group.html',
    "<md-content class=md-padding layout-padding flex> <section mb-preloading=ctrl.groupLoading layout=column md-whiteframe=1 layout-margin> <div layout=column layout-padding> <div layout=column layout-align=\"center start\"> <table class=\"amd-table amd-table-description\"> <tr> <td translate>ID</td> <td>{{group.id}}</td> </tr> <tr> <td translate>Name</td> <td> <am-wb-inline ng-model=group.name am-wb-inline-type=text am-wb-inline-label=\"Group name\" am-wb-inline-enable=app.user.owner am-wb-inline-on-save=save()> {{group.name || '...'}} </am-wb-inline> </td> </tr> <tr> <td translate>Description</td> <td> <am-wb-inline ng-model=group.description am-wb-inline-type=text am-wb-inline-label=\"Group description\" am-wb-inline-enable=app.user.owner am-wb-inline-on-save=save()> {{group.description || '...'}} </am-wb-inline> </td> </tr> </table> </div> </div> <div layout=column layout-gt-sm=row> <span flex></span> <md-button class=\"md-raised md-accent\" ng-click=remove()> <wb-icon>delete</wb-icon> <span translate>Delete</span> </md-button> </div> </section> <section mb-preloading=ctrl.roleLoading layout=column md-whiteframe=1 layout-margin> <h3 translate>Roles</h3> <md-chips ng-model=roles.items md-on-remove=removeRole($chip)> <md-chip-template> <strong>{{$chip.name}}</strong> </md-chip-template> </md-chips> <div ng-show=\"!roles.items || roles.items.length == 0\" layout=column layout-align=\"center center\"> <p translate>Group has no role.</p> </div> <div layout=row> <span flex></span> <md-button class=md-raised ng-click=changeRoles()> <wb-icon>edit</wb-icon> <span translate>Edit</span> </md-button> </div> </section> <section mb-preloading=ctrl.userLoading layout=column md-whiteframe=1 layout-margin> <h3 translate>Users</h3> <md-chips ng-model=users.items md-on-remove=removeUser($chip)> <md-chip-template> <strong>{{$chip.login}}</strong> </md-chip-template> </md-chips> <div ng-show=\"!users.items || users.items.length == 0\" layout=column layout-align=\"center center\"> <p translate>Group has no any member.</p> </div> <div layout=row> <span flex></span> <md-button class=md-raised ng-click=changeUsers()> <wb-icon>edit</wb-icon> <span translate>Edit</span> </md-button> </div> </section> </md-content>"
  );


  $templateCache.put('views/amd-groups.html',
    "<md-content mb-infinate-scroll=nextPage() layout=column mb-preloading=ctrl.working flex> <mb-pagination-bar mb-model=paginatorParameter mb-reload=reload mb-sort-keys=sortKeys mb-sort-keys-titles=sortKeysTitles mb-enable-search=true mb-more-actions=moreActions> </mb-pagination-bar>  <md-list flex ng-if=\"ctrl.items && ctrl.items.length !== 0\"> <md-list-item ng-repeat=\"group in ctrl.items track by group.id\" ng-href=\"{{'group/'+group.id}}\" class=md-3-line> <wb-icon>group</wb-icon> <div class=md-list-item-text layout=column> <h3>{{group.name}}</h3> <h4></h4> <p>{{group.description}}</p> <wb-icon class=md-secondary ng-click=remove(group) ng-show=remove aria-label=remove>delete</wb-icon> </div> <md-divider md-inset></md-divider> </md-list-item>  <div layout=column layout-align=\"center center\"> <md-progress-circular ng-show=ctrl.working md-diameter=96>{{'Loading ...' | translate}}</md-progress-circular> </div> </md-list> <div layout=column layout-align=\"center center\" ng-if=\"!ctrl.items || ctrl.items.length == 0\"> <h2>Empty group list</h2> <p>Any group match with the query. You can add a new group by click on the add button</p> <md-button ng-click=addGroup()> <wb-icon>add</wb-icon> <span translate>Add</span> </md-button> </div> </md-content>"
  );


  $templateCache.put('views/amd-role.html',
    "<md-content class=md-padding layout-padding flex> <section mb-preloading=ctrl.roleLoading layout=column layout-align=\"center start\" md-whiteframe=1 layout-margin> <table class=\"amd-table amd-table-description\"> <tr> <td translate>ID </td> <td>{{role.id}}</td> </tr> <tr> <td translate>Name</td> <td> <am-wb-inline ng-model=role.name am-wb-inline-type=text am-wb-inline-label=\"Role name\" am-wb-inline-enable=app.user.owner am-wb-inline-on-save=save()> {{role.name || '...'}} </am-wb-inline> </td> </tr> <tr> <td translate>Code name</td> <td> <am-wb-inline ng-model=role.code_name am-wb-inline-type=text am-wb-inline-label=\"Role code_name\" am-wb-inline-enable=app.user.owner am-wb-inline-on-save=save()> {{role.code_name || '...'}} </am-wb-inline> </td> </tr> <tr> <td translate>Application</td> <td> <am-wb-inline ng-model=role.application am-wb-inline-type=text am-wb-inline-label=\"Role application\" am-wb-inline-enable=app.user.owner am-wb-inline-on-save=save()> {{role.application || '...'}} </am-wb-inline> </td> </tr> <tr> <td translate>Description</td> <td> <am-wb-inline ng-model=role.description am-wb-inline-type=textarea am-wb-inline-label=\"Role description\" am-wb-inline-enable=app.user.owner am-wb-inline-on-save=save()> {{role.description || '...'}} </am-wb-inline> </td> </tr> </table> </section> <section mb-preloading=ctrl.groupLoading layout=column md-whiteframe=1 layout-margin> <h3>Groups</h3> <md-chips ng-model=groups.items md-on-remove=removeGroup($chip)> <md-chip-template> <strong>{{$chip.name}}</strong> <em>({{$chip.description}})</em> </md-chip-template> </md-chips> <div ng-show=\"!groups.items || groups.items.length == 0\" layout=column layout-align=\"center center\"> <p translate>There is no group having this role.</p> </div> <div layout=row> <span flex></span> <md-button class=md-raised ng-click=changeGroups()> <wb-icon>edit</wb-icon> <span translate>Edit</span> </md-button> </div> </section> <section mb-preloading=ctrl.userLoading layout=column md-whiteframe=1 layout-margin> <h3>Users</h3> <md-chips ng-model=users.items md-on-remove=removeUser($chip)> <md-chip-template> <strong>{{$chip.first_name}} {{$chip.last_name}}</strong> <em>({{$chip.login}})</em> </md-chip-template> </md-chips> <div ng-show=\"!users.items || users.items.length == 0\" layout=column layout-align=\"center center\"> <p translate>There is not user having this role.</p> </div> <div layout=row> <span flex></span> <md-button class=md-raised ng-click=changeUsers()> <wb-icon>edit</wb-icon> <span translate>Edit</span> </md-button> </div> </section> </md-content>"
  );


  $templateCache.put('views/amd-roles-of.html',
    "<md-content mb-infinate-scroll=nextPage() layout=column mb-preloading=ctrl.working flex> <mb-pagination-bar mb-model=paginatorParameter mb-reload=reload mb-sort-keys=sortKeys mb-more-actions=moreActions> </mb-pagination-bar> <md-list-item ng-repeat=\"role in ctrl.items\" ng-href=\"{{'role/'+role.id}}\" class=noright> <wb-icon>mood</wb-icon> <p>{{ role.name }} ({{role.description}})</p> <wb-icon class=md-secondary ng-click=remove(role) aria-label=remove>delete</wb-icon> </md-list-item> <div layout=column layout-align=\"center center\" ng-show=\"!ctrl.items || ctrl.items.length == 0\"> <h2>Empty roles list</h2> <p>Roles give permissions to user, group or other object to do something. Use the following actions to add a new role.</p> <md-button ng-click=add()> <wb-icon>add</wb-icon> <span translate>Add</span> </md-button> </div> </md-content>"
  );


  $templateCache.put('views/amd-roles.html',
    "<md-content wb-infinite-scroll=nextPage layout=column flex> <mb-pagination-bar mb-model=paginatorParameter mb-reload=reload mb-sort-keys=sortKeys mb-sort-keys-titles=sortKeysTitles mb-enable-search=true> </mb-pagination-bar>  <md-list flex> <md-list-item ng-repeat=\"role in ctrl.items\" ng-href=\"{{'role/'+role.id}}\" class=md-3-line> <wb-icon>accessibility</wb-icon> <div class=md-list-item-text layout=column> <h3>{{role.name}}</h3> <h4></h4> <p>{{role.description}}</p> </div> <md-divider md-inset></md-divider> </md-list-item>  <div layout=column layout-align=\"center center\"> <md-progress-circular ng-show=\"ctrl.status === 'working'\" md-diameter=96>{{'Loading ...' | translate}} </md-progress-circular> </div> </md-list> </md-content>"
  );


  $templateCache.put('views/amd-user-new.html',
    " <form name=ctrl.myForm flex layout=column ng-submit=\"addUser(model, ctrl.myForm)\" mb-preloading=ctrl.working> <input hide type=\"submit\"> <div style=\"text-align: center\" layout-margin ng-show=\"!ctrl.working && errorMessage\"> <p><span md-colors=\"{color:'warn'}\" translate>{{errorMessage}}</span></p> </div> <div layout=column layout-padding> <md-input-container> <label translate>First name</label> <input name=first_name ng-model=model.first_name> </md-input-container> <md-input-container> <label translate>Last name</label> <input name=last_name ng-model=model.last_name> </md-input-container> <md-input-container> <label translate>Username</label> <input name=login ng-model=model.login required> <div ng-messages=ctrl.myForm.login.$error md-auto-hide=false> <div ng-message=required translate>This field is required.</div> <div ng-message=unique translate>Username is used before.</div> <div ng-message=minlength translate>Username is too short.</div> </div> </md-input-container> <md-input-container> <label translate>Email</label> <input name=email ng-model=model.email type=email required> <div ng-messages=ctrl.myForm.email.$error md-auto-hide=false> <div ng-message=required translate>This field is required.</div> <div ng-message=notnull translate>This field is required.</div> <div ng-message=email translate>Email is not valid.</div> </div> </md-input-container> <md-input-container> <label translate>Password</label> <input name=password ng-model=model.password type=password required> <div ng-messages=ctrl.myForm.password.$error md-auto-hide=false> <div ng-message=required translate>This field is required.</div> <div ng-message=minlength translate>Password is too short.</div> </div> </md-input-container>          </div> <div layout=row> <span flex></span> <md-button class=md-raised ng-click=cancel()> <wb-icon>close</wb-icon> <span translate>Cancel</span> </md-button> <md-button class=md-raised ng-click=\"addUser(model, ctrl.myForm)\"> <wb-icon>done</wb-icon> <span translate>Add</span> </md-button> </div> </form>"
  );


  $templateCache.put('views/amd-user.html',
    "<md-content class=md-padding layout-padding flex>  <section mb-preloading=ctrl.userLoading layout=column md-whiteframe=1 layout-margin> <div layout=column layout-gt-sm=row> <img width=250px height=250px ng-src=\"/api/user/{{user.id}}/avatar\"> <div flex> <table class=\"amd-table amd-table-description\"> <tr> <td translate>ID</td> <td>{{user.id}}</td> </tr> <tr> <td translate>Username</td> <td>{{user.login}}</td> </tr> <tr> <td translate>First name</td> <td>{{user.first_name}}</td> </tr> <tr> <td translate>Last name</td> <td>{{user.last_name}}</td> </tr> <tr> <td translate>EMail</td> <td>{{user.email}} <a ng-if=user.email ng-href=mailto:{{user.email}}> <md-button class=\"md-icon-button md-warn\"><wb-icon>mail</wb-icon></md-button> </a> </td> </tr> <tr> <td translate>Joined date</td> <td>{{user.date_joined | pdate:'jYYYY/jMM/jDD'}}</td> </tr> <tr> <td translate>Last login</td> <td>{{user.last_login | pdate:'jYYYY/jMM/jDD'}}</td> </tr> <tr> <td translate>Activation state</td> <td> <span ng-if=!app.user.owner>{{user.active ? 'Active' : 'Deactive' | translate}}</span> <md-switch ng-if=app.user.owner ng-model=user.active ng-change=update()> {{user.active ? 'Active' : 'Deactive' | translate}} </md-switch> </td> </tr> </table> </div> </div> <div layout=column layout-gt-sm=row> <span flex></span> <md-button class=\"md-raised md-accent\" ng-click=remove()> <wb-icon>delete</wb-icon> <span translate>Delete</span> </md-button> </div> </section> <section mb-preloading=ctrl.roleLoading layout=column md-whiteframe=1 layout-margin> <h3 translate>Roles</h3> <md-chips ng-model=roles.items md-on-remove=removeRole($chip)> <md-chip-template> <strong>{{$chip.name}}</strong> </md-chip-template> </md-chips> <div ng-show=\"!roles.items || roles.items.length == 0\" layout=column layout-align=\"center center\"> <p translate>User has no role.</p> </div> <div layout=row> <span flex></span> <md-button class=md-raised ng-click=changeRoles()> <wb-icon>edit</wb-icon> <span translate>Edit</span> </md-button> </div> </section> <section mb-preloading=ctrl.groupLoading layout=column md-whiteframe=1 layout-margin> <h3 translate>Groups</h3> <md-chips ng-model=groups.items md-on-remove=removeGroup($chip)> <md-chip-template> <strong>{{$chip.name}}</strong> </md-chip-template> </md-chips> <div ng-show=\"!groups.items || groups.items.length == 0\" layout=column layout-align=\"center center\"> <p translate>User is not memeber of any group.</p> </div> <div layout=row> <span flex></span> <md-button class=md-raised ng-click=changeGroups()> <wb-icon>edit</wb-icon> <span translate>Edit</span> </md-button> </div> </section> </md-content>"
  );


  $templateCache.put('views/amd-users.html',
    "<md-content wb-infinate-scroll=nextPage layout=column flex>  <mb-pagination-bar mb-model=paginatorParameter mb-reload=reload mb-export=exportList mb-sort-keys=sortKeys mb-sort-keys-titles=sortKeysTitles mb-enable-search=true mb-more-actions=moreActions> </mb-pagination-bar> <md-list flex mb-preloading=\"ctrl.exporting || ctrl.status === 'working'\" ng-if=\"ctrl.items && ctrl.items.length !== 0\"> <md-list-item ng-repeat=\"user in ctrl.items track by user.id\" ng-href=\"{{'user/'+user.id}}\" class=md-3-line> <img ng-src=/api/user/{{user.id}}/avatar class=\"md-avatar\"> <div class=md-list-item-text layout=column> <h3>{{user.first_name}} - {{user.last_name}}</h3> <h4>{{user.login}}</h4> <p> <b><span translate>{{user.active ? 'Active' : 'Deactive'}}</span></b>&ensp; <span translate>Joined date</span>: {{user.date_joined | pdate:'jYYYY/jMM/jDD'}}&ensp; <span translate>Last Login</span>: {{user.last_login | pdate:'jYYYY/jMM/jDD'}} </p> </div> <wb-icon class=md-secondary ng-click=remove(user) ng-show=remove aria-label=remove>delete</wb-icon>  <md-divider md-inset></md-divider> </md-list-item>  <div layout=column layout-align=\"center center\"> <md-progress-circular ng-show=\"ctrl.status === 'working'\" md-diameter=96>{{'Loading ...' | translate}} </md-progress-circular> </div> </md-list> <div layout=column layout-align=\"center center\" ng-if=\"ctrl.status !== 'working' && !(ctrl.items && ctrl.items.length)\"> <h2>Empty user list</h2> <p>Any user match with the query. You can add a new user by click on the add button</p> <md-button ng-click=addUser()> <wb-icon>add</wb-icon> <span translate>Add</span> </md-button> </div> </md-content>"
  );

}]);
